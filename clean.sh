#!/bin/bash

# Get the directory of the script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Delete build and bin directory.
rm -rf build
rm -rf bin

# Delete the lock.
rm -f complete.lock
