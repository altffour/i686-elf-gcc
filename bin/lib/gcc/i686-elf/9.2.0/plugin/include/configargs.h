/* Generated automatically. */
static const char configuration_arguments[] = "/home/akham/Projects/i686-elf-gcc/gcc/configure --target=i686-elf --prefix=/home/akham/Projects/i686-elf-gcc/bin --disable-nls --enable-languages=c,c++";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "pentiumpro" } };
