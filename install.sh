#!/bin/bash

# Get the directory of the script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Prepare for build.
export PREFIX="$DIR/bin"
export TARGET=i686-elf
export PATH="$PREFIX/bin/:$PATH"

if [ -f "$DIR/complete.lock" ]; then
	# The complete.lock exists don't build, but add bin to PATH variable.
	echo "complete.lock exists."
	echo "To force build run ./clean.sh first."
	export PATH="$DIR/bin/bin/:$PATH"
	echo "Added bin to PATH"
	exit
fi

# Create directory structure.
mkdir -p build
mkdir -p build/binutils
mkdir -p build/gcc

# Build binutils first.
cd build/binutils
$DIR/binutils/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make -j8
make -j8 install

# Build gcc.
cd ../../
cd build/gcc
$DIR/gcc/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++
make -j8 all-gcc
make -j8 all-target-libgcc
make -j8 install-gcc
make -j8 install-target-libgcc

# Create lock
touch $DIR/complete.lock

# Set PATH variable.
export PATH="$DIR/bin/bin/:$PATH"
